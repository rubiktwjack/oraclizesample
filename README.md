# oraclizeSample

1. open cli and type ganache-cli
2. record first account
3. open cli and cd in ethereum-bridge
4. type npm install
5. type node bridge -H localhost:8545 -a 9 --dev
6. wait and record this line"OAR = OraclizeAddrResolverI(address);"
7. replace from: "0x719e33f6bd4c0c0aab3160f6380b45f55a9a73a2" with first account in javascript code
8. replace OAR = OraclizeAddrResolverI(0x6f485c8bf6fc43ea212e93bbf8ce046c7f1cb475); with new OAR in solidity code
9. use https://dapps.oraclize.it/browser-solidity to comoile solidity
10. replace abi and bytecode with new abi and bytecode in javascript code
11. open cli in folder and type node contract.js 